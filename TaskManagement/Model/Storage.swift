//
//  FileManager.swift
//  TaskManagement
//
//  Created by Shadi Ghelman on 4/4/24.
//

import Foundation

class Storage {
    static let shared = Storage()
    
    let fileManager: FileManager = .default
    var documentsURL: URL? { fileManager.urls(for: .documentDirectory, in: .userDomainMask).first }
    private func fileURL() throws -> URL {
        try documentsURL.unwrapped().appending(component: fileName)
    }
    private let fileName = "Tasks"
    
    func loadTasks() async throws -> [TaskItem] {
        try await Task {
            let data = try Data(contentsOf: fileURL())
            let decoder = PropertyListDecoder()
            return try decoder.decode([TaskItem].self, from: data)
        }.value
    }
    
    func saveTasks(_ tasks: [TaskItem]) async throws {
        let encoder = PropertyListEncoder()
        
        try await Task {
            let data = try encoder.encode(tasks)
            try data.write(to: fileURL())
        }.value
    }
}

extension Optional {
    enum Error: Swift.Error {
        case unexpectedNil
    }
    
    func unwrapped() throws -> Wrapped {
        guard let self else { throw Error.unexpectedNil }
        return self
    }
}
