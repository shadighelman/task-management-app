//
//  TaskManagerStore.swift
//  TaskManagement
//
//  Created by Shadi Ghelman on 4/4/24.
//

import Foundation

class TaskManagerStore: ObservableObject {
    init(tasks: [TaskItem] = []) {
        self.tasks = tasks
    }
    
    @Published var isRestoringTasks: Bool = false
    @Published var tasks: [TaskItem] = []
    @Published var taskToAdd: TaskItem = TaskItem()
    
    var storage = Storage()
    
    func toggle(taskID: TaskItem.ID) {
        if let index = tasks.firstIndex(where: { $0.id == taskID }) {
            tasks[index].isDone.toggle()
        }
    }
    
    func move(fromOffsets source: IndexSet, toOffset destination: Int) {
        tasks.move(fromOffsets: source, toOffset: destination)
    }

    func delete(atOffsets offsets: IndexSet) {
        tasks.remove(atOffsets: offsets)
    }
    
    func submit() {
        // Validation and Autocorrect
        if taskToAdd.title.isEmpty {
            if !taskToAdd.note.isEmpty {
                taskToAdd.title = "New Task"
                tasks.append(taskToAdd)
            }
        } else {
            tasks.append(taskToAdd)
        }
        clearInput()
        
        Task.detached { [store] in
            // Save tasks on disk
            await store()
        }
    }
    
    private func store() async {
        try? await storage.saveTasks(tasks)
    }
    
    @MainActor
    func restore() async {
        isRestoringTasks = true
        try? self.tasks = await storage.loadTasks()
        isRestoringTasks = false
    }
    
    func clearInput() {
        self.taskToAdd = TaskItem()
    }
}
