//
//  TaskItem.swift
//  TaskManagement
//
//  Created by Shadi Ghelman on 4/4/24.
//

import Foundation

struct TaskItem: Hashable, Equatable, Identifiable, Codable {
    var id = UUID()
    var title: String = ""
    var note: String = ""
    var isDone = false
}
