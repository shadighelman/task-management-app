//
//  TaskManagementApp.swift
//  TaskManagement
//
//  Created by Shadi Ghelman on 4/4/24.
//

import SwiftUI

@main
struct TaskManagementApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
