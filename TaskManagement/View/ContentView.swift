//
//  ContentView.swift
//  TaskManagement
//
//  Created by Shadi Ghelman on 4/4/24.
//

import SwiftUI

struct ContentView: View {
    
    @State private var isAddingNewTask = false
    @State private var filter = Filter.all
    @ObservedObject var store = TaskManagerStore()
    var tasks: [TaskItem] { store.tasks }
    
    var completedTasks: [TaskItem] { tasks.filter { $0.isDone } }
    var incompleteTasks: [TaskItem] { tasks.filter { !$0.isDone } }
    var visibleTasks: [TaskItem] {
        switch filter {
        case .completed: completedTasks
        case .incomplete: incompleteTasks
        case .all: tasks
        }
    }
    
    func listContent() -> some View {
        List {
            ForEach(visibleTasks) { task in
                TaskView(
                    title: task.title,
                    note: task.note,
                    isDone: task.isDone
                )
                .onTapGesture {
                    withAnimation {
                        store.toggle(taskID: task.id)
                    }
                }
            }
            .onMove(perform: store.move)
            .onDelete(perform: store.delete)
            
            if isAddingNewTask {
                MutableTaskView(
                    title: $store.taskToAdd.title,
                    note: $store.taskToAdd.note,
                    isDone: $store.taskToAdd.isDone
                )
                .onChange(of: isAddingNewTask, store.clearInput)
            }
        }
    }
    
    func filterMenu() -> some View {
        Menu("", systemImage: "ellipsis.circle") {
            ForEach(Filter.allCases, id: \.self) { filter in
                var image: String {
                    self.filter == filter
                    ? "checkmark"
                    : filter.systemImage
                }
                
                Button(
                    filter.rawValue.capitalized,
                    systemImage: image
                ) {
                    self.filter = filter
                }
            }
        }
    }
    
    var body: some View {
        NavigationStack {
            listContent()
                .task { await store.restore() }
                .navigationTitle(filter.title)
                .toolbar {
                    ToolbarItem(placement: .topBarTrailing) {
                        if isAddingNewTask {
                            Button("Done") { 
                                store.submit()
                                isAddingNewTask = false
                            }
                        } else {
                            filterMenu()
                        }
                    }
                    
                    ToolbarItem(placement: .bottomBar) {
                        if !isAddingNewTask {
                            Button {
                                isAddingNewTask.toggle()
                            } label: {
                                HStack {
                                    Image(systemName: "plus.circle.fill")
                                    Text("New Task")
                                }
                            }
                            .frame(
                                maxWidth: .infinity,
                                alignment: .leading
                            )
                        }
                    }
                }
            /// Loading indicator
                .redacted(reason: store.isRestoringTasks ? .placeholder : [])
                .disabled(store.isRestoringTasks)
                .overlay {
                    if store.isRestoringTasks {
                        ProgressView()
                    }
                }
        }
    }
}

#Preview {
    ContentView(
        store: TaskManagerStore(
            tasks: [
                TaskItem(title: "Task 1", note: "Note 1"),
                TaskItem(title: "Task 2", note: ""),
                TaskItem(title: "", note: "Note 3"),
            ]
        )
    )
}
