//
//  Filter.swift
//  TaskManagement
//
//  Created by Shadi Ghelman on 4/4/24.
//

enum Filter: String, Hashable, Equatable, Codable, CaseIterable {
    case completed
    case incomplete
    case all
    
    var systemImage: String {
        switch self {
        case .completed: "checklist.checked"
        case .incomplete: "checklist.unchecked"
        case .all: "checklist"
        }
    }
    
    var title: String {
        self.rawValue.capitalized
    }
}
