//
//  TaskView.swift
//  TaskManagement
//
//  Created by Shadi Ghelman on 4/4/24.
//

import SwiftUI

struct MutableTaskView: View {
    @Binding var title: String
    @Binding var note: String
    @Binding var isDone: Bool
    
    var body: some View {
        VStack(alignment: .leading, spacing: 0) {
            var systemImage: String {
                isDone ? "checkmark.circle.fill" : "circle"
            }
            Label(
                title: { TextField("", text: $title) },
                icon: { Image(systemName: systemImage) }
            )
            
            Label(
                title: {
                    TextField(
                        "Add Note",
                        text: $note,
                        prompt: Text("Add Note"),
                        axis: .vertical
                    )
                },
                icon: { Image("") }
            )
        }
        .frame(maxWidth: .infinity, alignment: .leading)
        .contentShape(Rectangle())
    }
}

#Preview {
    List {
        MutableTaskView(
            title: .constant("New Task"),
            note: .constant(""),
            isDone: .constant(true)
        )
        
        MutableTaskView(
            title: .constant(""),
            note: .constant(""),
            isDone: .constant(false)
        )
    }
}
