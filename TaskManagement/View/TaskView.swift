//
//  TaskView.swift
//  TaskManagement
//
//  Created by Shadi Ghelman on 4/4/24.
//

import SwiftUI

struct TaskView: View {
    let title: String
    let note: String
    let isDone: Bool
    
    var body: some View {
        VStack(alignment: .leading, spacing: 0) {
            Label(
                title,
                systemImage: isDone ? "checkmark.circle.fill" : "circle"
            )
            
            if !note.isEmpty {
                Label(
                    note,
                    systemImage: ""
                )
            }
        }
        .frame(maxWidth: .infinity, alignment: .leading)
        .contentShape(Rectangle())
    }
}

#Preview {
    List {
        TaskView(
            title: "Task 1",
            note: "Note 1",
            isDone: false
        )
    }
}
